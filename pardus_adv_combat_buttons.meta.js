// ==UserScript==
// @name        Advanced Combat Buttons
// @namespace   http://userscripts.xcom-alliance.info/
// @description Allows you to switch between Offensive, Balanced and Defensive combat modes without having to leave the navigation screen.
// @author      Miche (Orion) / Sparkle (Artemis)
// @include     http*://*.pardus.at/main.php*
// @include     http*://*.pardus.at/building.php*
// @include     http*://*.pardus.at/ship2ship_combat.php*
// @include     http*://*.pardus.at/ship2opponent_combat.php*
// @include     http*://*.pardus.at/overview_advanced_skills.php
// @version     2.1
// @updateURL 	http://userscripts.xcom-alliance.info/adv_combat_buttons/pardus_adv_combat_buttons.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/adv_combat_buttons/pardus_adv_combat_buttons.user.js
// @icon 		http://userscripts.xcom-alliance.info/adv_combat_buttons/icon.png
// @grant       GM_xmlhttpRequest
// @grant       GM_getValue
// @grant       GM_setValue
// ==/UserScript==
